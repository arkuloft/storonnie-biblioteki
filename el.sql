-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eloquent
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'2006-08-02 19:16:26','Donny Dare','2P2!My0]ko@$','Omnis incidunt voluptas sit repudiandae est suscipit laudantium. Consequatur accusantium omnis illum iure cumque ad suscipit.','2016-03-11 18:10:39'),(3,'1986-10-17 05:19:10','Dorothy Turner',']*}wu);P4kCN[iTu/kM,','Ullam modi asperiores accusamus itaque voluptatem ullam. Quia rem blanditiis quia tempore natus nostrum rerum. Vitae exercitationem qui magni quae sit.','2016-03-11 18:10:39'),(4,'2015-08-17 01:16:02','Erik Tillman',':8~wOy=_]wPQc$gW#`','Est non molestias ullam beatae. A dolor magnam est sed occaecati ex.','2016-03-11 18:10:39'),(5,'1999-12-27 21:43:48','Julien Thompson','YI{yD6CI\'','Magnam quasi consectetur molestiae quod quas architecto. Possimus temporibus rerum ut. Eligendi molestiae laboriosam quia.','2016-03-11 18:10:39'),(6,'1998-09-27 22:57:42','Dr. Cassandra Yundt I','G]G:FFE>t','Ut est dolorem dolorem accusamus iusto itaque. Consequuntur temporibus sit ab id et. Eius ipsam quae dolores aspernatur est eaque fugit.','2016-03-11 18:10:39'),(7,'1991-11-09 18:43:49','Dr. Ransom Baumbach PhD',')<yETnX>p\\','Eum ipsum tempore quam. Consequuntur omnis explicabo consequatur ullam aliquam. Quibusdam ipsa quia nobis dolore necessitatibus.','2016-03-11 18:10:39'),(8,'2015-10-16 06:54:18','Prof. Judson Douglas','/d]7qL1R','Quia numquam dolorum pariatur vel fugit voluptatum. Velit ut autem quas dicta accusantium atque suscipit. Error repellendus ut et vitae est ut rerum.','2016-03-11 18:10:39'),(9,'1970-10-16 20:16:27','Edmund Koch','ekrOR>l_<7K','Maiores deserunt ut atque aliquam. Enim reprehenderit quia suscipit esse. Ut numquam non enim est explicabo. Impedit totam qui quia quod alias aut distinctio.','2016-03-11 18:10:39'),(10,'1991-02-23 11:07:45','Prof. Chesley Tillman Jr.','Z$y+zT','Corporis debitis omnis et corrupti consectetur ratione in temporibus. Aut soluta ut non tenetur eaque et quam. Omnis accusantium odio est.','2016-03-11 18:10:39'),(11,'2011-10-05 20:59:08','Brenna Jast','@Vs\\dTX/','Perspiciatis corrupti repudiandae magnam cum a sit. Quasi enim sed in natus. Consectetur dolores nihil nemo eum esse.','2016-03-11 18:10:39'),(12,'1977-04-06 18:33:57','Dr. Oran Gutmann Jr.','B(sIw5?in9sB$L`\'*e','Sit ab eum rerum. Voluptate eaque et cum eum est ut. Officiis ullam qui voluptatibus perspiciatis sed.\nCum deleniti dignissimos non molestias cumque harum. Illum et repellendus dolorem est eveniet.','2016-03-11 18:10:39'),(13,'1982-04-02 00:58:29','Annalise Crooks','KtwL07WZ+es','Voluptates ut iusto iure sit iste et molestiae. Dolorem impedit ut voluptate tenetur.','2016-03-11 18:10:39'),(14,'1995-09-27 02:10:30','Lizeth Pagac','E[-D!b|','Aperiam enim ea culpa sed sit error. Et esse quisquam amet dolorum consequuntur. Ex deleniti voluptatem sit doloribus sapiente soluta.','2016-03-11 18:10:39'),(15,'1984-12-24 23:59:22','Prof. Pattie Hagenes','tIO`FYijfY','Laudantium earum voluptas impedit et ut. Nam minima rerum autem vel quas. Libero eius quaerat et voluptatum id sed.','2016-03-11 18:10:39'),(16,'1997-03-23 15:14:26','Jeanette DuBuque','myM{6SfKE7[oaMZ','Eum minima inventore dolores voluptatem et dolorem. Eos repellendus optio earum fugiat repudiandae fuga sunt labore. Dolor nostrum quia iste alias maxime ad ut.','2016-03-11 18:10:39'),(17,'1972-02-03 01:22:26','Vesta Brakus','r7&Yr4i!','Aut pariatur corporis commodi aliquid possimus ut. Repellendus quo id animi alias non nihil. Dignissimos amet est maxime nesciunt laborum. Suscipit non qui vel.','2016-03-11 18:10:39'),(18,'1993-12-11 08:09:22','Turner Larson','N^wOapceSsS}[Ox\"wp','Aliquid est iusto accusantium delectus nemo quia minima minima. Tempora fugiat autem sit. Dolorem amet modi vel qui. Voluptatem et placeat architecto pariatur laudantium voluptatibus deleniti.','2016-03-11 18:10:39'),(19,'2011-10-27 09:10:11','Dr. Pink Trantow DVM','7SgonP>[q0&GeclZ!','Est dolor et vitae. Architecto molestiae optio quia a. Quis ut quo earum voluptate quidem. Magnam rerum et distinctio doloremque. Sit qui rerum deleniti perferendis velit nostrum dolores.','2016-03-11 18:10:39'),(20,'1970-07-15 21:14:19','Dr. Ronaldo Pfeffer','yz=X?/\\-CxW','Ullam recusandae molestiae a soluta. Qui quidem doloribus magnam quae autem. Esse fugiat rerum sunt.','2016-03-11 18:10:39'),(21,'1988-12-13 16:30:12','Justyn Kihn','E.W%wM]&=R','Incidunt voluptatem ratione atque autem. Quisquam aperiam sit aut laboriosam nostrum commodi. Excepturi nisi laborum in tempore qui officia.','2016-03-11 18:10:39'),(22,'2004-06-09 02:15:33','Ellie Waelchi MD','1~p)T^@iv\\;`','Explicabo sed consectetur facilis enim. Aut quas doloribus dolorem dolorem sint id praesentium. Molestiae ullam minus quod nisi saepe voluptatem atque.','2016-03-11 18:10:39'),(23,'1970-04-10 11:53:41','Mrs. Alana Trantow','U]<UWWtVX','Dolorem itaque ut quia ea. Rerum velit debitis soluta eum culpa est. Enim in provident et nesciunt. Debitis ipsam omnis illum.','2016-03-11 18:10:39'),(24,'1991-01-17 21:24:49','Jaida Morissette MD','L#/vbSYXUQ*\'#x','A similique aut sit ut iure harum ipsa. Minus ducimus enim non minima. Velit ducimus consequatur est nihil qui nemo ut.','2016-03-11 18:10:39'),(25,'1995-08-10 21:16:44','Robyn Oberbrunner PhD','\'\"_lE.np](gr{4','Iste dolorem cum vel. Quia voluptatem nam delectus voluptatem laboriosam.','2016-03-11 18:10:39'),(26,'2009-09-05 21:36:48','Frankie Stracke','Cm)]d9*7n','Sit eveniet omnis illo incidunt. Consectetur et ea ab odio. Dolores sint voluptas et iste doloremque nihil ut modi. Sit sed enim doloribus nesciunt ad aut.','2016-03-11 18:10:39'),(27,'2011-09-26 09:45:57','Maureen Osinski','*R\'X-pdJ^','Id ut accusamus id occaecati molestiae. Provident quia repellendus est sint aliquam molestias ab. Est sed est rerum officia beatae. Quidem qui mollitia ut.','2016-03-11 18:10:39'),(28,'2003-05-30 06:18:46','Jeffry Harber','kTv|/K$?:MZ.]','Suscipit eum aut consequatur rem aliquid quasi eum nam. Alias quis et earum maxime dignissimos. Et ad ipsa reiciendis similique eius. Voluptatem cum et dignissimos deleniti.','2016-03-11 18:10:39'),(29,'1999-12-19 04:43:11','Rosemary Kuvalis','lT<7U5','Quae rerum qui error laudantium. Ab non commodi molestiae qui non. Labore et aspernatur veritatis itaque consequatur occaecati placeat. Praesentium voluptate quia eveniet.','2016-03-11 18:10:39'),(30,'2011-04-14 18:17:33','Hillard Fahey','!59CuU6=~Y','Delectus porro non consectetur quod velit. Sunt molestias maxime est vel libero quis natus. Aut in atque similique dignissimos ratione labore. Magnam ducimus est est velit sunt nesciunt.','2016-03-11 18:10:39'),(31,'1984-01-04 09:38:29','Margret Schimmel','K%^h{Z_g&xOSXG16n>S','Maxime voluptatum eos aut unde. Esse totam et est fugit praesentium quae est. Quibusdam officiis est et et velit veritatis aut in.','2016-03-11 18:10:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-12 20:00:21
